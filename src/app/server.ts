import Fastify from "fastify";
const app = Fastify({
  logger: true,
});

app.get("/", async function handler(_request, _response) {
  return { hello: "world" };
});

try {
  app.listen({ port: 3000 }, (err, address) => {
    console.log(`server listening at${address}`);
  });
} catch (err) {
  app.log.error(err);
  process.exit(1);
}
